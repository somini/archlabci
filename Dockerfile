FROM archlinux/base:latest

# Sync the local pacman database to be able to install packages
RUN pacman --noconfirm -Sy
# Update the system, the image might be stale
RUN pacman --noconfirm -Syu
# Include ALL dependencies, the image lacks "base-devel"
RUN pacman --noconfirm -S base-devel
RUN pacman --noconfirm -S namcap ninja graphviz

# Create a user to run the builds (makepkg mostly)
RUN mkdir /var/cache/homedirs
RUN useradd --base-dir=/var/cache/homedirs --create-home --shell=/bin/nologin builduser && usermod -L builduser
RUN echo 'builduser ALL = NOPASSWD: /usr/bin/pacman' >>/etc/sudoers
RUN visudo -c
RUN sudo -l -U builduser

# Include non-"base-devel" packages, but very used anyway
RUN pacman --noconfirm -S git
RUN pacman --noconfirm -S python python-pip
