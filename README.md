# ArchLabCI: Bulding Arch Linux packages on GitLab

GitLab has all the necessary infrastructure to create a ArchLinux repository from scratch.

- Store the PKGBUILDS on the repository
- Use CI to build the packages
- Serve the repository using Gitlab Pages

This project should support just that.

## Usage

Bootstrap the `build.ninja` file. This is a ninja invocation based on
`bootstrap.ninja`.

```sh
$ bin/bootstrap
```

Once that's done, you can use all the ninja capabilities. To build all packages, use:

```sh
$ ninja all-packages
```

To build a specific package by name (including all dependencies), use:

```sh
$ ninja PKGBUILD/$name
```

To build all packages and create the repository, use simply:

```sh
$ ninja
```
