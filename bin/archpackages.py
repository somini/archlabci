# TODO: Move this to a proper package?
from pathlib import Path
from dataclasses import dataclass
import logging
import subprocess

logger = logging.getLogger(__name__)


@dataclass
class Package:
    """
    "Internal" class, used to store the common package data and useful for
    serialization. This is location, independent.
    """
    name: str

    def __post_init__(self):
        # Development Package
        self.development = False
        for suffix in ['bzr', 'git', 'hg', 'svn']:
            if self.name.endswith(f'-{suffix}'):
                self.development = True
                break


@dataclass
class PackageLocation:
    """
    `Package`, plus location-specific data.
    """
    package: Package
    topdir: Path
    priority: int = 0

    def __post_init__(self):
        name = self.package.name
        # TODO: Migrate to a "folder" namedtuple?
        self.folder_package = self.topdir / name
        # TODO: Migrate to a "file" namedtuple?
        self.file_pkgbuild = self.folder_package / 'PKGBUILD'
        self.file_install = self.topdir / f'{name}.install'
        self.file_args = self.topdir / f'{name}.args'

        # Priority
        if self.file_install.exists():
            # TODO: Read the priority from the ".install" file contents
            self.priority = 1

    @property
    def filelist(self):
        absolute_topdir = self.topdir.resolve(strict=False)
        # logger.debug(f'makepkg @ {self.folder_package}')
        with subprocess.Popen(['makepkg', '--packagelist'],
                              cwd=self.folder_package,
                              stdout=subprocess.PIPE, text=True) as process:
            for raw_line in process.stdout:
                line = raw_line.strip()
                # logger.debug(f'- {line}')
                fname = Path(line)
                yield self.topdir / fname.relative_to(absolute_topdir)


def get_ploc(topdir: Path, name: str):
    package = Package(name)
    logger.info(f'=> {package.name}')
    ploc = PackageLocation(package, topdir)
    if ploc.priority != 0:
        logger.info(f'   - Priority: {ploc.priority}')
    return ploc


def get_plocs(topdir: Path):
    for rpath in topdir.iterdir():
        if rpath.is_dir():
            # TODO: Move this to the PackageLocation class
            pkgbuild = rpath / 'PKGBUILD'
            pkgname = rpath.name
            if pkgbuild.is_file():
                yield get_ploc(topdir, pkgname)
            else:
                logger.debug(f'Not a Package: {pkgname}')


def sort_plocs(packages):
    def sort_key(ploc: PackageLocation):
        # Sort By:
        # - Reverse Priority (More is First)
        # - Name
        # See also: https://docs.python.org/3/howto/sorting.html#sort-stability-and-complex-sorts
        return (ploc.priority * -1, ploc.package.name)
    return sorted(packages, key=sort_key)
