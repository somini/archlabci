TOPDIR := PKGBUILD

.PHONY: all

all: build.ninja

build.ninja:
	bin/build-ninja -v $(TOPDIR) >$@

.PHONY: build
build: build.ninja
	ninja

.PHONY: clean-all clean clean-repos clean-ninja

clean-all: clean clean-ninja

clean:
	$(RM) repository/*

clean-repos:
	git submodule foreach 'git checkout .'

clean-ninja:
	ninja -v -t clean
	$(RM) build.ninja
	$(RM) .ninja_deps .ninja_log
